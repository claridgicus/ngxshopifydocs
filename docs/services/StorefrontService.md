# Storefront Service

Members of the Template Service

## GraphQL Query

### Run a GraphQL Query

Run a graphQL query using the existing connection

```ts
runGraphqlQuery(query, variables);
```

### Run a GraphQL Mutation

Run a GraphQL mutation using the existing connection

```ts
runGraphqlMutation(mutation, variables);
```

## Customer

### Get a customer and its relations

Gets the current customers addresses, full customer record & orders

```ts
customerDetails();
```

## Auth

### Login as a customer

```ts
customerAccessTokenCreate(customerAccessTokenCreateInput: CustomerAccessTokenCreateInput)
```

### Create as a cusotmer

Create a customer, typically used in sign up

```ts
customerCreate(createUser: CustomerCreateInput)
```

### Reset a password

Password reset input

```ts
customerReset(resetUser: CustomerResetInput)
```

### Reset a password by email URL

Password reset input by email link URL

```ts
customerResetByUrl(resetUrl: string, password: string)
```

### Ask for a password reset

Ask for a reset password authentication

```ts
customerRecover(recoverUser: CustomerRecoverInput)
```

## Address Management

### Set a customers default address

Send the address I to set the customers Default

```ts
customerDefaultAddressUpdate(addressId: number)
```

### Update a customers address

Update address by ID

```ts
customerAddressUpdate(id: number, address: MailingAddressInput)
```

### Create a customers address

Create an address on a customer

```ts
customerAddressCreate(address: MailingAddressInput)
```

## Orders

### Get an Order by its ID

This functionality doesn't work currently due to Shopify handling returning orders in the most inefficient way ever

```ts
orderById(id);
```
