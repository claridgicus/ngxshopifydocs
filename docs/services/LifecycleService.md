# Lifecycle Service

## NgxShopify Request Headers

This utility is required to make calls to the Liquid API as XHR expects a response type, this informs the global interceptor as to what to expect

```ts
    ngxHttpRequestHeaders(type: 'text' | 'json', updatePageTitle?: boolean): HttpHeaders {
        let headers = new HttpHeaders()
        headers = headers.set('ngxShopify', type)
        if (updatePageTitle) {
            headers = headers.append('ngxShopifyTitle', updatePageTitle.toString())
        }

        return headers
    }
```

## NgxShopify Headers No Error

This utility prevents NgxShopify from producing a 404 error on this particular request. This is useful for when you're arbitrarily pulling in product data for template purposes

```ts

    ngxHttpRequestNoErrorPage(headers: HttpHeaders): HttpHeaders {
        return headers.append('ngxShopify404', 'true')
    }
```

## Unescape HTML

This utility returns an unescape HTML string. If the liquid template contains "| escape", this is required

```ts

     */
    unescapeHtml(unsafe:string): SafeHtml {
    }

```

```ts
localstorageClear();
```

```ts
    async loadScript(name: string):
    {
      script: string,
      loaded: boolean,
      status: string
    }
```
