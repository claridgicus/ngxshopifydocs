# Services

Let's talk about services!

## [Template Service](/services/TemplateService.html)

The go to place for retrieving data relating to products, theme settings, collections, etc.

Features things like:

- Get the Front Page
- Get a Page
- Get A Product
- Get a Collection
- Search
- Interact with Modals, Drawers & Lightboxes
- Listen to menu states

## [Cart Service](/services/CartService.html)

Adding, removing or viewing your cart: all in one place!

- Get the cart
- Show/hide the minicart
- Add, update, remove an item from cart
- Keeps an active cart during the session and is constantly up to date with super quick load times

## [Account Service](/services/AccountService.html)

Login and manage your customers state with the account service

- Login
- Logout

## [Storefront Service](/services/StorefrontService.html)

Our least favourite Shopify service, we use this to handle the account functionality of Shopify, and nothing else.

- Run a GraphQL query to Shopify's storefront using the existing link
- View / Update customer information
- Address management

## [Lifecycle Service](/services/LifecycleService.html)

Handles the smaller practicalities such as local development and setting the correct headers


## [Authentication Guard](/services/AuthGuard.html)

An authentication guard to allow authenticated users into areas of your site