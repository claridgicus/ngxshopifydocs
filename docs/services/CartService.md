# Cart Service

## Show the minicart
  Emits true to the minicart observable
 
  ```ts
  showMiniCart(): void;
  ```
## Hide the minicart  
  Emits false to the minicart observable
 
  ```ts
  hideMiniCart(): void;
  ```
## Clear all items from cart
  Runs /cart/clear.js, and emits a clean cart to the cart observable
 
  ```ts
  clearCart(): void;
  ```
  
## Add a product to cart
 Update the quantity of a variantId in the cart
   
  ```ts
  updateQuantity(variantId: number, quantity: number): Promise<void>;
  ```