# Template Service

Members of the Template Service

## Informational Pages

### Get the frontpage from Shopify

```ts
getFrontPage(): Observable<any>
```

### Get the page from Shopify by its handle

```ts
getPage(pageHandle: string, title?: boolean)
```

## Product

### Get a product page by its handle

```ts
getProductPage(productHandle: string, title?: boolean): Observable<Product>
```

Produces 404 error if not found

### Get a product by its handle

```ts
getProduct(productHandle: string, title?: boolean): Observable<Product>
```

Does not produce 404 if product is not found

## Collection

### Get collections

```ts
getCollectionsPage(page?: string, title?: boolean): Observable<Collection>
```

### Get collection page by handle

```ts
getCollectionPage(collectionHandle: string, page?: string, title?: boolean)
```

### Get collection by handle

```ts
getCollection(collectionHandle: string, page?: string, title?: boolean)
```

Does not redirect user to 404 page if not found

### Get collection by Vendor

```ts
getVendorCollectionPage(vendor: string, page?: string, title?: boolean)
```

### Get collection by Product Type

```ts
getProductTypeCollectionPage(type: string, page?: string, title?: boolean)
```

## Blogs

### Get a blog article from a blog by its handle

```ts
getBlogPage(handle?: string, title?: boolean)
```

### Get a blog by its handle

```ts
getBlogsPage(category: string, page?: string, title?: boolean)
```

## Search

### Get search results from the Shopify Autocomplete Search API

```ts
getSearchResults(search, title?: boolean)
```

## Misc

### Get an Instagram feed

Provided a token is specified you can request posts from the Instagram user linked to the token

```ts
getInstagramFeed(accessToken: string, posts: string = '6')
```

### Product recommendations for a given Product ID

```ts
getRecommendationsByProductId(productId: number | string, limit: number | string)
```

## Theme Helpers

## Drawers

A drawer is a slide out area used for content and info

### Show the theme's drawer

```ts
showDrawer(drawer: Drawer)
```

### Hide the theme's drawer

```ts
hideDrawer();
```

## Modals

A modal is a small window that overlays the original tab, presenting new information without the need for a new link or tab

### Show the theme's modal

```ts
showModal(modal: Modal)
```

### Hide the theme's modal

```ts
hideModal();
```

## Lightboxes

A lightbox is an overlay that allows an image to be seen in increased size and detail

### Show the theme's lightbox

```ts
showLightbox(number?: number)
```

### Hide the theme's lightbox

```ts
hideLightbox();
```

## SemiHeadless

### Process a Shopify menu

This function processes the NgxShopify Menu format into something more usable

```
@description parses a link_list object and splits the corresponding
URL into a URL & a Params Object
This allows NgxShopify to set query strings in the menu links
if so required in a routerlink [router]
@example [routerLink]="[ baseUrl, params ]
```

```ts
processMenu(menu): Menu
```

### Unescape HTML

```ts
unescapeHtml(unsafe);
```

Responses from the NgxShopify Liquid files can contain escaped HTML, pass the escaped HTML string through this function and receive the HTML

### Turn an escaped query string into a JS Object

```ts
escapedQueryStringToJSON(entry);
```
