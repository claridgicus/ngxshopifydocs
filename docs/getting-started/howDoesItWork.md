# How does it work?

## NgxShopify - SemiHeadless

- Uses a combination of Angular tech and specialised JSON liquid templates to provide information to the frontend in blobs of data only
- Angular handles all the templating and routing required, leaving you free to create whatever you like
- All the data calls you would expect can be found on the various services provided by the NgxShopify library

## NgxShopify - Headless

- Heavily cached data cluster that proxies/caches the Shopify data source, far faster than Shopify
- Server Side Rendering, increase performance and SEO capabilities
- Uses the same basic api as the NgxShopify SemiHeadless implementation for minimal migration

[Talk to Us](mailto:james@claridge.com.au)
