# Installing NgxShopify for the first time

::: danger

**IMPORTANT: Back up your theme before running these commands as it will overwrite the theme. Theme preview is currently not supported as this is a feature in development. We recommend creating a dev store.**
:::

If your store does not currently have the NgxShopify theme installed, complete the following configuration files.

## Prerequisites

- [ ] Read the full 'getting started' guide before commencing
- [ ] Install Themekit: [Get it here](https://shopify.github.io/themekit/)
- [ ] Create a Shopify dev store
- [ ] Create a Shopify app with the required permissions
- [ ] Clone the starter theme repo: [Get it here](https://gitlab.com/claridgicus/ngxshopifystarter)
- [ ] Configure config.yml
- [ ] Configure environment.ts
- [ ] Configure environment.prod.ts
- [ ] Run the setup commands

<br>

**You require a private app that has:**

- [x] Theme read/write
- [x] Storefront access

<br>

## Steps

### 1. Clone the Starter Theme

```sh
git clone git@gitlab.com/claridgicus/ngxshopifystarter
```

### 2. Install Angular CLI

```
npm i -g @angular/cli
```

### 3. Configure Storefront YML

::: tip
These files have examples in the required locations with the suffix .example
eg environment.prod.example.ts
:::

```yaml
# config.yml
development:
  password: PASSWORD HERE
  store: ngxshopdemo.myshopify.com
  timeout: 3m0s
```

### 4. Configure Angular Environments

```ts
// ngxshopify/src/environments/environment.ts
export const environment = {
  apiurl: "https://YOURSTORE.myshopify.com/",
  production: false,
  minicart: false,
  cartCount: {
    dot: false,
    counter: true
  },
  debugHttp: true,
  debugLiquid: true,
  instagram: "YOURINSTAGRAMTOKEN",
  storefrontKey: "YOURSTOREFRONTKEY",
  externalScripts: [
    {
      name: "script",
      src: "fullurltoscript"
    }
  ]
};
```

```ts
// ngxshopify/src/environments/environment.prod.ts
export const environment = {
  apiurl: "/",
  production: true,
  minicart: false,
  cartCount: {
    dot: false,
    counter: true
  },
  debugHttp: true,
  debugLiquid: true,
  instagram: "YOURINSTAGRAMTOKEN",
  storefrontKey: "YOURSTOREFRONTKEY",
  externalScripts: [
    {
      name: "script",
      src: "fullurltoscript"
    }
  ]
};
```

<br>

then run

```bash
$ npm i
$ npm run setup:firstrun
```

<br>
<div class="text-center" ><h2>Congratulations - You're up and running!</h2>
<span><b>Now for the fun stuff</b></span></div>
<br>
<br>

## Set up your local for an existing project

NgxShopify is an angular framework used to build on top of Shopify either natively using the liquid CDN or in a full Headless configuration.

NgxShopify uses a combination of Shopify data sources in order to provide the quickest experience possible for users. This means we take full advantage of Liquid, AJAX & Storefront APIs.

We've taken care of most of the grunt work for you.

## Prerequisites

- [ ] Read the full "getting started" guide before commencing
- [ ] Install Themekit: [Get it here](https://shopify.github.io/themekit/)
- [ ] Find the Shopify app with the required permissions and use its details to fill in the configs
- [ ] Clone the repo that has your project
- [ ] Configure config.yml
- [ ] Configure environment.ts
- [ ] Configure environment.prod.ts
- [ ] Run the setup commands

<br>

```yaml
# config.yml
development:
  password: YOURTOKENHERE
  store: ngxshopdemo.myshopify.com
  timeout: 3m0s
```

### Configure Angular Environments

```ts
// ngxshopify/src/environments/environment.ts
export const environment = {
  apiurl: "https://YOURSTORE.myshopify.com/",
  production: false,
  minicart: false,
  cartCount: {
    dot: false,
    counter: true
  },
  debugHttp: true,
  debugLiquid: true,
  instagram: "YOURINSTAGRAMTOKEN",
  storefrontKey: "YOURSTOREFRONTKEY",
  externalScripts: [
    {
      name: "script",
      src: "fullurltoscript"
    }
  ]
};
```

```ts
// ngxshopify/src/environments/environment.prod.ts
export const environment = {
  apiurl: "https://YOURSTORE.myshopify.com/",
  production: true,
  minicart: false,
  cartCount: {
    dot: false,
    counter: true
  },
  debugHttp: true,
  debugLiquid: true,
  instagram: "YOURINSTAGRAMTOKEN",
  storefrontKey: "YOURSTOREFRONTKEY",
  externalScripts: [
    {
      name: "script",
      src: "fullurltoscript"
    }
  ]
};
```

    npm i && npm run setup && npm run start

in the root directory to start your own local instance of NgxShopify

then navigate to:

    http://localhost:4200

## Theme Structure

We have kept the underlying theme structure similar to the standard liquid theme you would be used to interacting with.
Here's a quick breakdown:

```sh
.
├─ assets # DELETED EVERY BUILD | this is where ngxShopify js files are hosted and deployed
├─ config # Standard theme settings_data.json, settings_schema.json
├─ layout # Custom ngxShopify theme templates are included here
├─ ngxShopify
│  ├─ e2e # testing
│  ├─ src
│  │  ├─ assets # keep your fonts and static images in this folder
│  │  ├─ environments # environment files for where angular looks for data
│  │  ├─ app
│  │  │  ├─ animations # keep your animations for reuse here
│  │  │  ├─ classes # custom classes for your own use
│  │  │  ├─ components # large pieces of reusable functionality (similar to shopify Sections)
│  │  │  ├─ layout # page layouts (similar to shopify Layouts)
│  │  │  ├─ pages # pages such as the product page / collection page (similar to shopify Pages)
│  │  │  ├─ services # reusable functions that are made available theme wide can be located here
│  │  │  └─ snippets # similar to shopify snippets, small chunks of reusable functionality
│  ├─ app-routing.module.ts # contains the standard routes angular is allowed to handle, unless the app runs in full headless mode modification is not recommended
│  ├─ app.module.ts # declares all the useable parts of the application
│  ├─ ngxshopify.component.ts # this is the bootstrapping file
│  ├─ index.html # the local version of theme.liquid
│  └─ styles.scss # sitewide scss sheet, this should be used just for colours, fonts and button/link styling
├─ sections # custom ngxShopify JSON templates with a trailing comma
├─ snippets # custom ngxShopify JSON objects eg "Product" "Collection" objects
└─ templates # custom ngxShopify JSON page templates, reference corresponding sections
```

## Deployment

To deploy the theme to production, run:

    npm run deploy

This will build the theme and check for errors and then deploy the theme to the assets folder. It will then deploy the contents of the assets folder to production.
