module.exports = {
    title: "NgxShopify | Angular Shopify built for Agencies, delivering for Clients",
    description: "Docs for the most maintainable Shopify Headless Framework around",
    dest: "public",
    themeConfig: {
        nav: [
            { text: "Home", link: "/" },
            { text: "Get Started", link: "/getting-started/" },
            { text: "Services", link: "/services/" },
            // { text: "Components", link: "/services/" },
            // { text: "Themeing", link: "/services/" },
            { text: "Guides", link: "/guides/" },
            { text: "FAQS", link: "/faqs/" },

            { text: "NgxShopify.com", link: "https://ngxshopify.com" },
            { text: "Component Store", link: "https://ngxshopify.com" }
        ],
        searchPlaceholder: "Search the docs",
        smoothScroll: true,
        sidebarDepth: 6,
        sidebar: {
            "/getting-started/": ["", "howDoesItWork"],
            "/services/": [
                "",
                "TemplateService",
                "StorefrontService",
                "CartService",
                "AccountService",
                "LifecycleService"
            ],
            "/guides/": [
                "",
                "MakeAPageTemplate",
                "ShopifyContactForm",
                "AddingAssets",
                "AddingAWebfont"
            ],
            "/faqs/": ["", "SEO"],
            "/": [""]
        },
        home: true,
        logo: "/assets/ngx_logo_resized.svg"
    },
    markdown: {
        extendMarkdown: md => {
            // use more markdown-it plugins!
            md.use(require("markdown-it-task-checkbox"), { disabled: false });
        }
    }
};