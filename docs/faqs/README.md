# Frequently Asked Questions

## How to deploy NgxShopify

run `npm run deploy` in the root of the install to deploy a compiled version of the angular app

## Can I use theme watch?

We don't recommend you use shopify `theme watch` as the folder structure and size consumes exorbitant cpu power to maintain an open terminal with watch, part of the beauty of NgxShopify is your ability to run it locally by running `npm run start` in the install root, this will allow you to develop on localhost:4200 without deploying breaking changes to production