---
home: true
heroImage: /assets/ngx_logo.svg
heroText: Welcome to NgxShopify's SemiHeadless Documentation!
tagline: We're building the best Angular Shopify Theme we can, enabling agencies to take full advantage of previous work by constructing totally reusable code. Run it natively on the Shopify Liquid API.
actionText: Get Started in 5 minutes! →
actionLink: /getting-started/
features:
  - title: Simplicity first
    details: Download a starter theme, with everything you need to go out of the box, get started with SemiHeadless in under 5 minutes!
  - title: Developer focused
    details: No more will your developers have to understand a whole theme as we use a component based approach so that your devs can modify just the section they require.
  - title: Performant
    details: Angular loads everything required for you, presenting one of the most compatible solutions available.
  - title: Fully documented & typed library functions
    details: NgxShopify is fully JSDoc Hinted for your developers convenience, all objects and repsonses are typed in TypeScript for ease of use.
  - title: Implement functionality in minutes
    details: Buy components/theme integrations from our component store and implement them in seconds, watch as they are styled as per your theme's existing styleguide.
  - title: SemiHeadless / Headless
    details: NgxShopify allows your team to utilise Shopify's Liquid API and theme editor to run in "SemiHeadless" mode, fully headless SSR operation is coming soon!
footer: MIT Licensed | Copyright © 2018-present NgxShopify
---
