# Adding a web font

## Steps

## 1. Add the font to the assets folder

Add the font to the assets folder of Ngxshopify folder

## 2. Write the fontface tag on the theme.liquid file

Include the webfont in the theme liquid file inside a fontface by referencing the name of the font using the liquid assets filter
::: v-pre
`{{myfont | asset_url}}`
:::

Deploy the theme.liquid file using `theme deploy templates/theme.liquid`

This will let the Ngxshopify deploy script copy the files into the assets folder and then deploy them to Shopify

::: tip
You cannot keep a folder structure inside the assets folder of Ngxshopify which is a shopify assets limitation.
:::
