# Creating a Shopify Contact Form

::: tip
You must ask Shopify to disable Recaptcha on form submission.
Contact them through Live Chat and ask them to "Disable Recaptcha on Form Submission"
:::

## Implement a Contact Form

The below is a stripped back and easily customisable example form for you to implement

```ts
import { Component, Input } from "@angular/core";
import { TemplateService } from "ngx-shopify";

@Component({
  selector: "contact-form",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ComponentContactForm {
  success: boolean;
  form: {
    name: string;
    phone: string;
    email: string;
    question: string;
  };

  constructor(private templateService: TemplateService) {
    this.form = { name: "", phone: "", email: "", question: "" };
  }

  submit() {
    let form = new FormData();
    form.append("utf8", "✓");
    form.append("form_type", "contact");
    form.append("contact[name]", this.form.name);
    form.append("contact[phone]", this.form.phone);
    form.append("contact[email]", this.form.email);
    form.append("contact[question]", this.form.question);
    this.templateService.submitForm(form).subscribe(
      (data: any) => {
        this.form = { name: "", phone: "", email: "", question: "" };
        this.success = true;
      },
      err => {
        console.log(err);
      }
    );
  }
}
```

```html
<input type="text" [(ngModel)]="form.name" placeholder="Name" required />
<input
  type="text"
  [(ngModel)]="form.email"
  placeholder="Email"
  [email]="true"
  required
/>
<input type="text" [(ngModel)]="form.phone" placeholder="Phone" required />
<textarea
  type="text"
  [(ngModel)]="form.question"
  placeholder="Question"
  required
></textarea>
<button (click)="submit()" type="submit">Submit</button>
<div *ngIf="success">Thanks for contacting us, we will be in touch!</div>
```
