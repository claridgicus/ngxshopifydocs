# Adding an asset

## Steps

## Step 1 - Add the asset to the assets folder

Add the asset to the assets folder of NgxShopify
 
```sh
. 
└─ ngxShopify
  └─ assets 
    └─ yoursweetfile.gif
.
```

## Step 2 - Modify the shop.liquid file

Include the asset in the shop.liquid file inside the json blob by referencing the name of the font using the liquid assets filter:
`shop.liquid`
```liquid 
"myAssetName": "{{myasset | asset_url}}"
```

Please make sure to make note of the semicolons and ensure that the file produces valid JSON

Deploy the shop.liquid file using `theme deploy` command on the respective file

This will let the NgxShopify deploy script copy the files into the assets folder and then deploy them to Shopify

::: tip
You cannot keep a folder structure inside the assets folder of NgxShopify which is a shopify assets limitation.
:::

## Step 3 - Include the asset throughout the store

You can now utilise the asset throughout the theme by importing the store object from the template service.

```ts
Import { TemplateService } from 'ngx-shopify'

@Component()
export class ComponentWhereYouWantTheAsset {
    store
    constructor(private templateService: TemplateService) {
       this.store = this.templateService.store
    }

}
```

```html
<img [src]="store.shop.myasseturl" />
```
