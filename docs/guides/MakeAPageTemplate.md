# Adding a new Page Template

::: tip
We assume a basic level of knowledge in adding components to angular, however if you need help here is a great reference:
[Learn how components work in Angular](https://codeburst.io/how-to-create-use-angular-7-components-6f8e8d575c3)
:::

We will quickly layout the basics of how to interact with the Liquid API and the process of configuring Angular to handle multiple page templates.

## What we will cover

In this tutorial we will cover:

- Adding a new Liquid Page Template and Sections
- Creating a matching Angular Component
- Running a deployment

First it's good practise to start angular by running `npm run start` in the root folder of your install. Angular will normally tell you any errors during compilation which can assist you in debugging.

## Steps

## 1. Create & Deploy a new Liquid Page Template

Create a new page.##custom##.liquid file and deploy it to Shopify

`page.custom.liquid`

```JSON
{
  "title": "{{page.title}}",
  "type": "##custom##",
  "settings": "{{page.content.settings | json }}",
  "metafields": {}
}
```

This file can be modified but we recommend you keep a consistent format between your Liquid files to make it nice and easy for dev. You can of course include a section as seen above, provided the section mimics the JSON format, which will allow NgxShopify to display a section on this page template of the theme customiser for you. We provide an example 'contact us' form and FAQ page with our starter theme to get you going.

## 2. Create a matching component

Now it's time to handle the data source in Angular!

### 2.1 Create a component

Create a matching component in `/ngxShopify/src/app/pages/page/##Your Component Here##`

Its contents should look something like this:

```html
<!-- /ngxShopify/src/app/pages/page/##Your Component Here##/page-##Your Component Here##.html -->

<h1>{{ page.title }}</h1>
<pre>{{page.settings | json}}</pre>
```

```ts
// /ngxShopify/src/app/pages/page/##Your Component Here##/page-##Your Component Here##.ts

import { Component, Input } from "@angular/core";

@Component({
  selector: "page-custom",
  templateUrl: "./page-custom.component.html",
  styleUrls: ["./page-custom.component.scss"]
})
export class PageCustomComponentHere {
  @Input() page: any;
}
```

###2.2 Add the component to the Barrel

Add the new page into the Barrel file for pages

```ts
// /ngxShopify/src/app/pages/page/index.ts

...
export * from './page.component'
export * from './page/page.component'
export * from './contact/contact.component'
export * from './##Custom Component Here/page-##Your Component Here##.ts
...
```

::: tip

A Barrel is a file that is used as a single entry point for a number of components. We try to organise your theme to a standard schema using Barrels to identify the various groups of components.

:::

### 2.3 Adding to the App Module file

Last step in creation of a component is to add it to the App Module file

```ts
// /ngxShopify/src/app/app.module.ts

...
@NgModule({
    declarations: [
		...
		Pages.PageParentPage,
		Pages.PageCustomComponentHere
		Pages.PageContact,
		...
	],
	....

```

This tells Angular that you would like to compile this component with the rest of the app!
Now the component is available for us to use anywhere on the site!

## 3. Modify the parent page component

Currently you've made both the parts required to handle the data, but we haven't included a way to access this template.

We find it straightforward using a parent component to handle child components with multiple layouts. This parent component is included in the router and whenever a "Page" is loaded it receives the JSON object and then renders the appropriate component.

Take a minute to familiarise yourself with the component.

This step requires knowledge of Angular's Input() function - as you see in the example above. The Input() function allows us to pass a data object between a parent component and its child components.

In the component `/ngxShopify/src/app/pages/page/page.component.html`
add your new Page component,
Instruct Angular on what variable to look for (in this instance "type") then, pass the component the page object using its input [page].

```html
<div class="page mh-80">
  <ng-container *ngIf="!loading  &&  page" [ngSwitch]="page.type">
    <page-##custom##
      [page]="page"
      ngSwitchCase="'##custom##'"
    ></page-##custom##>
    <page-faq [page]="page" ngSwitchCase="'faq'"></page-faq>
    <page-contact [page]="page" ngSwitchCase="'contact'"></page-contact>
    <page-page [page]="page" ngSwitchDefault></page-page>
  </ng-container>
</div>
```

::: tip

Debugging this step is easy. If your component is not appearing, you can dump the current object to the screen or console, in both the parent and the child to make sure data is passing through correctly <pre>{{page | json}}</pre>

:::

## 4. Deploy NgxShopify

In one terminal window run a deploy `npm run deploy`
In another terminal window, use theme deploy to ship only the Liquid files you have modified.

## 5. Create a matching page in the CMS

Go into Shopify admin and assign a new page to this new Liquid layout, then you can use the theme customiser to populate any sections it may have.

## 6. Job done, get busy!

Navigate to the page you created on localhost:4200

`localhost:4200/pages/mycustompage`

You can now customise and style your component by selecting the new page you've created through the theme editor and even add sections to it if you desire. We have provided a couple of basic pages to get you started so just copy what we have laid out for you!
